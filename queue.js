let collection = [];

// Write the queue functions below.

function print(){
    console.log(collection);
    return collection;
}

function enqueue(item) {

   return collection.push(item);
}

function dequeue(){
    return collection.shift();
}

function front(){
    return collection[0];
}

function size(){

    return collection.length;
}

function isEmpty(){

    if(collection.length === 0){
        return true;
    } else {
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};



